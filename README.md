# ExpertSystem

~~~~
# - Comment
# - Input to ExpertSystem program

A => B                          # if A, then B
A + !B => F                     # A + !B is "A and not B"
A + !B | F => G                 # !B | F is "not B or F"
A + !B | F | C => E
A + !B | F | C ^ G  => H        # ^ - XOR
!A | !E | F | C ^ G  => I
B ^ !A | C => J
(B ^ !A) | C => K

=ABCD       #initially set to True
?EFGHIJK    #should be resolved
~~~~