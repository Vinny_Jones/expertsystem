#!/bin/zsh
autoload -U colors && colors

#Will work assuming program was properly built with CLion

isMatch() {
    inputFile=$1
    expectedFile=$2

    if [ -f $expectedFile ]; then
        value=$(cat $expectedFile)
    else
        echo "Expected results file [$expectedFile] not found"
        return 0
    fi

    if [[ $value == "" ]]; then
        echo "Empty expected results"
		return 0
	fi

    #### Linux
    #output=$(timeout 3 ../cmake-build-debug/ExpertSystem $inputFile || >&2 echo "Error: Timeout for command" && return 0)
    ##### MacOS
    output=$(../cmake-build-debug/ExpertSystem $inputFile)

    if [[ $output =~ "$value"  ]]; then
        return 1
    else
        return 0
    fi
}

for file in $(pwd)/*.input; do
    filename=$(basename -- "$file")
    filename="${filename%.*}"

    expectedFile="$filename.expected"
    
    if ( isMatch $file $expectedFile )
    then
        echo "$fg[red]Test FAILED$reset_color [$filename]"
    else
        echo "$fg[green]Test PASSED$reset_color [$filename]"
    fi
done
