/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IExpression.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:29:27 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:29:31 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IEXPRESSION_HPP
# define IEXPRESSION_HPP

# include <iostream>
# include <list>
# include <memory>
# include <sstream>
# include "ExpressionError.hpp"

enum class eExprResult {
	True,
	False,
	Undefined
};

enum class eExprType {
	Fact,
	Unary,
	Binary
};

enum class eOperation {
	NOT,
	AND,
	OR,
	XOR
};

class IExpression {
public:
    typedef std::list<std::shared_ptr<IExpression>> ExpressionList;

    static std::string      resultToString(eExprResult result); /* String representation of result value */

    IExpression() = default;
    IExpression(const IExpression & rhs) = default;
    IExpression &   operator=(const IExpression & rhs) = default;
    virtual ~IExpression() = default;

	virtual eExprType       type() const = 0;               /* Expression type */
	virtual bool            isDefined() const = 0;         	/* Returns true if was set by rule or by initial fact */
    virtual std::string     toString() const = 0;           /* String representation of expression */
    virtual ExpressionList  getChildren() = 0;              /* Return child nodes of IExpression */
    virtual void            setInBrackets(bool);            /* Set true if expression should be in brackets */
	virtual eExprResult     getExprResult() const = 0;      /* Calculates result (for lhs expression) */
	virtual void            setExprResult(eExprResult) = 0; /* Sets expression result (for rhs expression) */

protected:
    bool            m_inBrackets = false;
};

typedef std::shared_ptr<IExpression>    ExpressionPtr;
typedef std::list<ExpressionPtr>        ExpressionList;     /* Same as above only because of CLion issue */

#endif
