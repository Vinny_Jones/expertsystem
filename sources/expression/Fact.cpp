/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fact.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:29:17 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:29:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fact.hpp"

Fact::Fact(char id) : m_state(eExprResult::False), m_isset(false), m_id(id) {}

eExprType       Fact::type() const {
	return (eExprType::Fact);
}

bool            Fact::isDefined() const {
	return (m_isset);
}

std::string     Fact::toString() const {
    std::ostringstream  result;

    result << std::string(1, m_id);
    return (result.str());
}

ExpressionList  Fact::getChildren() {
    ExpressionList  empty;
    return (empty);
}

eExprResult     Fact::getExprResult() const {
	return (m_state);
}

void            Fact::setExprResult(eExprResult state) {
	if (!m_isset) {
		m_state = state;
		if (state != eExprResult::Undefined)
			m_isset = true;
		return;
	}
	if (state != m_state && state != eExprResult::Undefined) {
	    std::ostringstream  stream;

	    stream << "Contradiction for fact " << toString() << ". Previously set value ";
	    stream << IExpression::resultToString(m_state) << " is now changing to " << IExpression::resultToString(state);

        throw ExpressionError(stream.str());
	}

}
