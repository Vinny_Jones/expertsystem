/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BinaryExpression.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:28:45 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:28:48 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "BinaryExpression.hpp"

BinaryExpression::BinaryExpression(const ExpressionPtr & lhs, const ExpressionPtr & rhs, eOperation operation) {
	if (lhs == nullptr || rhs == nullptr)
		throw std::runtime_error("[Internal][BinaryExpression] Both of binary expressions must be non-null");
	m_expr.first = lhs;
	m_expr.second = rhs;

	switch (operation) {
	case eOperation::AND:
	case eOperation::OR:
	case eOperation::XOR:
		m_operation = operation;
		break;
	default:
		throw std::logic_error("[Internal][BinaryExpression] Unknown binary expression");
	}
}

BinaryExpression::BinaryExpression(const ExpressionPtr & first, const ExpressionPtr & second, const BinaryExpression & rhs)
        : BinaryExpression(rhs)
{
    m_expr.first = first;
    m_expr.second = second;
}

eExprType   BinaryExpression::type() const {
	return (eExprType::Binary);
}

bool        BinaryExpression::isDefined() const {
	if (m_operation == eOperation::OR && getExprResult() == eExprResult::True) {
		return (m_expr.first->isDefined() || m_expr.second->isDefined());
	}
	return (m_expr.first->isDefined() && m_expr.second->isDefined());
}

std::string BinaryExpression::toString() const {
    std::ostringstream  result;

    result << (m_inBrackets ? "(" : "") << m_expr.first->toString();
    switch (m_operation) {
    case eOperation::AND:
        result << " and ";
        break;
    case eOperation::OR:
        result << " or ";
        break;
    case eOperation::XOR:
        result << " xor ";
        break;
    default:
        throw std::runtime_error("[Internal][toString] Unhandled operation type of Binary expression");
    }
    result << m_expr.second->toString() << (m_inBrackets ? ")" : "");
    return (result.str());
}

ExpressionList  BinaryExpression::getChildren() {
    ExpressionList  list { m_expr.first, m_expr.second };
    return (list);
}

eExprResult BinaryExpression::getExprResult() const {
	eExprResult lhs = m_expr.first->getExprResult();
	eExprResult rhs = m_expr.second->getExprResult();

	switch (m_operation) {
	case eOperation::OR:
		if (lhs == eExprResult::True || rhs == eExprResult::True)
			return (eExprResult::True);
		if (lhs == eExprResult::Undefined || rhs == eExprResult::Undefined)
			return (eExprResult::Undefined);
		return (eExprResult::False);
	case eOperation::XOR:
		if (lhs == eExprResult::Undefined || rhs == eExprResult::Undefined)
			return (eExprResult::Undefined);
		if (lhs != rhs)
			return (eExprResult::True);
		return (eExprResult::False);
	case eOperation::AND:
		if (lhs == eExprResult::True && rhs == eExprResult::True)
			return (eExprResult::True);
		if (lhs == eExprResult::False || rhs == eExprResult::False)
			return (eExprResult::False);
		return (eExprResult::Undefined);
	default:
		throw std::runtime_error("[Internal][getExprResult] Unhandled operation type of Binary expression");
	}
}

void        BinaryExpression::setExprResult(eExprResult result) {
	switch (m_operation) {
	case eOperation::XOR:
		setExclusiveLogic(result);
		break;
	case eOperation::OR:
		if (result == eExprResult::False) {
			m_expr.first->setExprResult(eExprResult::False);
			m_expr.second->setExprResult(eExprResult::False);
		} else {
			setExclusiveLogic(result);
		}
		break;
	case eOperation::AND:
		if (result == eExprResult::True) {
			m_expr.first->setExprResult(eExprResult::True);
			m_expr.second->setExprResult(eExprResult::True);
		} else {
			setExclusiveLogic(result);
		}
		break;
	default:
		break;
	}

}

void        BinaryExpression::setExclusiveLogic(eExprResult result) {
	bool        isDefinedLhs = m_expr.first->isDefined();
	bool        isDefinedRhs = m_expr.second->isDefined();

	if ((!isDefinedLhs && !isDefinedRhs) || result == eExprResult::Undefined) {
		m_expr.first->setExprResult(eExprResult::Undefined);
		m_expr.second->setExprResult(eExprResult::Undefined);
		return;
	}

	if (isDefinedLhs && isDefinedRhs && getExprResult() != result) {
	    throw ExpressionError("Contradiction of facts within OR logic");
	}

	ExpressionPtr   defined = isDefinedLhs ? m_expr.first : m_expr.second;
	ExpressionPtr   undefined = isDefinedLhs ? m_expr.second : m_expr.first;
	eExprResult     defResult = defined->getExprResult();

	switch (m_operation) {
	case eOperation::XOR:
		if (result == eExprResult::True) {
			undefined->setExprResult(defResult == eExprResult::True ? eExprResult::False : eExprResult::True);
		} else {
			undefined->setExprResult(defResult);
		}
		return;
	case eOperation::OR:
		if (result == eExprResult::True) {
			undefined->setExprResult(defResult == eExprResult::True ? eExprResult::Undefined : eExprResult::True);
		} else {
			defined->setExprResult(eExprResult::False);
			undefined->setExprResult(eExprResult::False);
		}
		return;
    case eOperation::AND:
        /* True and Undefined should be handled by here */
        if (result != eExprResult::False)
            throw std::runtime_error("Error [setExclusiveLogic]. Unexpected result for AND");
        undefined->setExprResult(defResult == eExprResult::True ? eExprResult::False : eExprResult::Undefined);
        return;
	default:
		break;
	}

	/* All cases should be handled by here */
	throw std::logic_error("[setExclusiveLogic] Unhandled case");
}
