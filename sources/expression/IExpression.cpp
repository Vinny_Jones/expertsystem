/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IExpression.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:29:40 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:29:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "IExpression.hpp"

std::string IExpression::resultToString(eExprResult result) {
    switch (result) {
    case eExprResult::True:
        return ("True");
    case eExprResult::False:
        return ("False");
    case eExprResult::Undefined:
        return ("Undefined");
    }
}

void        IExpression::setInBrackets(bool inBrackets) {
    m_inBrackets = inBrackets;
}
