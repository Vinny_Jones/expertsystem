/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fact.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:29:09 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:29:11 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FACT_HPP
# define FACT_HPP

# include "IExpression.hpp"

class Fact : public IExpression {
public:
	explicit Fact(char id);
	Fact(const Fact & rhs) = default;
	Fact &  operator=(const Fact &) = default;
	~Fact() final = default;

	eExprType       type() const override;
	bool            isDefined() const override;
    std::string     toString() const override;
    ExpressionList  getChildren() override;
	eExprResult     getExprResult() const override;
	void            setExprResult(eExprResult state) override;

private:
	eExprResult m_state;
	bool        m_isset;
	char        m_id;
};

typedef std::shared_ptr<Fact>   FactPtr;
typedef std::list<FactPtr>      FactList;

#endif
