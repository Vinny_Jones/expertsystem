/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   UnaryExpression.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:30:16 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:30:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "UnaryExpression.hpp"

UnaryExpression::UnaryExpression(const ExpressionPtr & ptr, eOperation operation) : m_expr(ptr) {
	if (ptr == nullptr)
		throw std::runtime_error("unable to create empty unary expression");
	switch (operation) {
	case eOperation::NOT:
		m_operation = operation;
		break;
	default:
		throw std::logic_error("unknown unary expression");
	}
}

UnaryExpression::UnaryExpression(const ExpressionPtr &ptr, const UnaryExpression &rhs) : UnaryExpression(rhs) {
    m_expr = ptr;
}

eExprType       UnaryExpression::type() const {
	return (eExprType::Unary);
}

bool            UnaryExpression::isDefined() const {
	return (m_expr->isDefined());
}

std::string     UnaryExpression::toString() const {
    std::ostringstream  result;

    result << (m_inBrackets ? "(" : "");
    switch (m_operation) {
    case eOperation::NOT:
        result << "not " << m_expr->toString();
        break;
    default:
        throw std::runtime_error("Unhandled operation type of Unary expression");
    }
    result << (m_inBrackets ? ")" : "");
    return (result.str());
}

ExpressionList  UnaryExpression::getChildren() {
    ExpressionList  children { m_expr };
    return (children);
}

eExprResult     UnaryExpression::getExprResult() const {
	eExprResult result = m_expr->getExprResult();

	if (m_operation == eOperation::NOT) {
		switch (result) {
		case eExprResult::True:
			return (eExprResult::False);
		case eExprResult::False:
			return (eExprResult::True);
		default:
			return (eExprResult::Undefined);
		}
	}
	throw std::runtime_error("Unhandled operation type of Unary expression");
}

void            UnaryExpression::setExprResult(eExprResult result) {
	switch (result) {
	case eExprResult::True:
		m_expr->setExprResult(eExprResult::False);
		break;
	case eExprResult::False:
		m_expr->setExprResult(eExprResult::True);
		break;
	default:
		m_expr->setExprResult(eExprResult::Undefined);
	}
}
