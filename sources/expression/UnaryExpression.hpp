/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   UnaryExpression.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:30:06 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:30:09 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UNARYEXPRESSION_HPP
# define UNARYEXPRESSION_HPP

# include "IExpression.hpp"

class UnaryExpression final : public IExpression {
public:
	UnaryExpression(const ExpressionPtr & ptr, eOperation operation);
	UnaryExpression(const UnaryExpression & rhs) = default;
    UnaryExpression(const ExpressionPtr & ptr, const UnaryExpression & rhs);
	UnaryExpression &   operator=(const UnaryExpression & rhs) = default;
	~UnaryExpression() final = default;

	eExprType       type() const override;
	bool            isDefined() const override;
	std::string     toString() const override;
	ExpressionList  getChildren() override;
	eExprResult     getExprResult() const override;
	void            setExprResult(eExprResult) override;

private:
	eOperation      m_operation;
	ExpressionPtr   m_expr;
};

#endif
