/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BinaryExpression.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:28:57 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:29:00 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BINARYEXPRESSION_HPP
# define BINARYEXPRESSION_HPP

# include "IExpression.hpp"

class BinaryExpression final : public IExpression {
public:
	BinaryExpression(const ExpressionPtr & lhs, const ExpressionPtr & rhs, eOperation operation);
	BinaryExpression(const BinaryExpression & rhs) = default;
    BinaryExpression(const ExpressionPtr & first, const ExpressionPtr & second, const BinaryExpression & rhs);
	BinaryExpression &   operator=(const BinaryExpression & rhs) = default;
	~BinaryExpression() final = default;

	eExprType       type() const override;
	bool            isDefined() const override;
	std::string     toString() const override;
	ExpressionList  getChildren() override;
	eExprResult     getExprResult() const override;
	void            setExprResult(eExprResult result) override;

private:
	void                                    setExclusiveLogic(eExprResult result);
	eOperation                              m_operation;
	std::pair<ExpressionPtr, ExpressionPtr> m_expr;
};

#endif
