/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InputError.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:27:57 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:28:06 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUTERROR_HPP
# define INPUTERROR_HPP

# include <stdexcept>
# include <string>

class InputError final : public std::runtime_error {
public:
    InputError() = delete;
    explicit InputError(const char * what);
    explicit InputError(std::string && what);
    InputError(const InputError & rhs) = default;
    InputError &    operator=(const InputError & rhs) = default;
    ~InputError() override = default;
};

#endif
