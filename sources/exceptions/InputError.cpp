/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InputError.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:27:44 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:27:46 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InputError.hpp"

InputError::InputError(const char * what) : std::runtime_error(what) {}

InputError::InputError(std::string && what) : std::runtime_error(what) {}
