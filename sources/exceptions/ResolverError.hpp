/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ResolverError.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:28:27 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:28:31 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RESOLVERERROR_HPP
# define RESOLVERERROR_HPP

# include <stdexcept>
# include <string>

class ResolverError : public std::runtime_error {
public:
    ResolverError() = delete;
    explicit ResolverError(const char * what);
    explicit ResolverError(std::string && what);
    ResolverError(const ResolverError & rhs) = default;
    ResolverError &    operator=(const ResolverError & rhs) = default;
    ~ResolverError() override = default;
};

#endif
