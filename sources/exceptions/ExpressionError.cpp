/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ExpressionError.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:27:11 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:27:14 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ExpressionError.hpp"

ExpressionError::ExpressionError(const char * what) : std::runtime_error(what) {}

ExpressionError::ExpressionError(std::string && what) : std::runtime_error(what) {}
