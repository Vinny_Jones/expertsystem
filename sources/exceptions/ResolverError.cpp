/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ResolverError.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:28:18 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:28:20 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ResolverError.hpp"

ResolverError::ResolverError(const char * what) : std::runtime_error(what) {}

ResolverError::ResolverError(std::string && what) : std::runtime_error(what) {}
