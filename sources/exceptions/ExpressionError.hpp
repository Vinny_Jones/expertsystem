/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ExpressionError.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:27:23 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:27:29 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXPRESSIONERROR_HPP
#define EXPRESSIONERROR_HPP

# include <stdexcept>
# include <string>

class ExpressionError : public std::runtime_error {
public:
    ExpressionError() = delete;
    explicit ExpressionError(const char * what);
    explicit ExpressionError(std::string && what);
    ExpressionError(const ExpressionError & rhs) = default;
    ExpressionError &   operator=(const ExpressionError & rhs) = default;
    ~ExpressionError() override = default;
};

#endif
