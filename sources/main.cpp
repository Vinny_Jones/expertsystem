/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:25:59 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:26:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InputParser.hpp"
#include "InteractiveMode.hpp"
#include "Resolver.hpp"

int     main(int argc, const char * argv[]) {
    bool    interactiveFlag = false;

    if (argc == 3 && std::string(argv[1]) == "-i") {
        interactiveFlag = true;
    } else if (argc != 2) {
        std::cout << "Usage: ./ExpertSystem [-i] filename" << std::endl;
        return 0;
    }

    try {
        Resolver::instance()->run(InputParser::instance()->parseFile(argv[interactiveFlag ? 2 : 1], interactiveFlag));

        while (interactiveFlag) {
            if (InteractiveMode::instance()->run())
                Resolver::instance()->run(std::move(InputParser::instance()->input()));
            else
                interactiveFlag = false;
        }
    } catch (InputError & ex) {
        std::cout << "An error occurred while reading from input: " << ex.what() << std::endl;
    } catch (ResolverError & ex) {
        std::cout << "An error occurred while resolving input: " << ex.what() << std::endl;
    } catch (ExpressionError & ex) {
        std::cout << "An error occurred while performing expression operation: " << std::endl << ex.what() << std::endl;
    } catch (std::exception & ex) {
        std::cout << "An error occurred: " << ex.what() << std::endl;
    }
	return (0);
}
