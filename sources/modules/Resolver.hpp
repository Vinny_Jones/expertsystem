/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Resolver.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:31:48 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:31:49 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RESOLVER_HPP
# define RESOLVER_HPP

# include <unordered_map>
# include "InputParser.hpp"
# include "ResolverError.hpp"

class Resolver final {
public:
	Resolver(const Resolver & rhs) = delete;
	Resolver &	operator=(const Resolver & rhs) = delete;
	virtual ~Resolver() = default;

	static Resolver *   instance();

	void                run(InputPtr && input);

private:
	Resolver() = default;

	void    processQueue();
	void    processFact(FactPtr & currentFact);
	void    printFactComparison(eExprResult oldValue, eExprResult newValue, std::string &&expr, std::string &&fact) const;
	bool    isInExpression(FactPtr &fact, ExpressionPtr &expr);
    void    mapExpressionToFacts(ExpressionPtr &rule, ExpressionPtr &current);


	static std::unique_ptr<Resolver>            m_instance;
	InputPtr                                    m_input;
	FactList                                    m_factQueue;
    std::unordered_map<ExpressionPtr, FactList> m_ruleFactMap;
    std::unordered_map<FactPtr, ExpressionList> m_processedRulesMap;
};

#endif
