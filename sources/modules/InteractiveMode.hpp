/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InteractiveMode.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:31:04 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:31:11 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERACTIVEMODE_HPP
# define INTERACTIVEMODE_HPP

# include "InputParser.hpp"

enum class eModeState {
    ADD_RULE,
    DELETE_RULE,
    EDIT_FACTS,
    EDIT_QUERIES,
    RUN,
    EXIT
};

class InteractiveMode final {
public:
    InteractiveMode(const InteractiveMode &) = delete;
    InteractiveMode &operator=(const InteractiveMode &) = delete;
    virtual ~InteractiveMode() = default;

    static InteractiveMode *    instance();
    bool                        run();

private:
    static std::unique_ptr<InteractiveMode> m_instance;

    InteractiveMode() = default;
    void        performAddRule();
    void        performDeleteRule();
    void        performEditFactList(InputParser::eReadingState state);
    eModeState  getUserCommand() const;
    void        showCommandsList() const;

};

#endif
