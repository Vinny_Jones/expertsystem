/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InputParser.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:30:53 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:30:55 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <BinaryExpression.hpp>
#include <UnaryExpression.hpp>
#include "InputParser.hpp"

std::unique_ptr<InputParser>    InputParser::m_instance = nullptr;
const std::string               InputParser::m_allowedFacts = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const std::string               InputParser::m_allowedInput = InputParser::m_allowedFacts + "?()!+|^<=>";

InputParser *   InputParser::instance() {
    if (m_instance == nullptr)
        m_instance.reset(new InputParser());
    return (m_instance.get());
}

InputPtr        InputParser::parseFile(const char *fileName, bool isInteractive) {
    std::ifstream   fileStream(fileName, std::ios::in);
    m_state = eReadingState::Rules;
    m_input = std::make_unique<tInput>();
    m_stringNumber = 0;

    if (fileStream.is_open()) {
        std::string     inputBuffer;

        while (std::getline(fileStream, inputBuffer)) {
            m_stringNumber++;
            if (fileStream.fail()) {
                throw std::ios_base::failure("Failed to read from input source.");
            }
            try {
                prepareRawInputString(inputBuffer);
                if (inputBuffer.empty())
                    continue;
                proceedInputString(inputBuffer);
            } catch (InputError & ex) {
                throw InputError(std::string("[line ") + std::to_string(m_stringNumber) + "] " + ex.what());
            }
        }
        fileStream.close();
    } else {
        throw std::logic_error("Unable to open " + std::string(fileName) + ".");
    }
    proceedFinalChecks();
    return (isInteractive ? createDeepCopy() : std::move(m_input));
}

void            InputParser::prepareRawInputString(std::string &str) {
    auto    startCommentPosition = str.find('#');

    if (startCommentPosition != std::string::npos) {
        str.erase(str.begin() + startCommentPosition, str.end());
    }
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    size_t  errPosition = str.find_first_not_of(m_allowedInput);
    if (errPosition != std::string::npos)
        throw InputError(std::string("Syntax error at <") + str[errPosition] + ">");
}

void            InputParser::proceedInputString(std::string & str) {
    switch (m_state) {
    case eReadingState::Rules:
        if (str[0] == '=') {
            m_state = eReadingState::Facts;
        } else {
            parseRule(str);
            break;
        }
    case eReadingState::Facts:
        parseFactString(str, m_state);
        m_state = eReadingState::Queries;
        break;
    case eReadingState::Queries:
        parseFactString(str, m_state);
        if (str[0] != '?')
            throw InputError(std::string("Invalid queries string - [") + str + "]");
        m_state = eReadingState::Finished;
        break;
    default:
        throw std::runtime_error("[Internal error][proceedInputString] Unknown state ");
    }
}

void            InputParser::parseRule(std::string &str) {
    size_t  divider = (str.find("<=>") == std::string::npos) ? str.find("=>") : str.find("<=>");

    if (divider == std::string::npos)
        throw InputError("Cannot find '<=>' or '=>' operator");
    std::string lhs = str.substr(0, divider);
    std::string rhs = str.substr(str.find('>') + 1);
    m_temp.clear();

    m_input->ruleList.emplace_back(parseExpression(lhs), parseExpression(rhs));
    if (str[divider] == '<')
        m_input->ruleList.emplace_back(m_input->ruleList.back().second, m_input->ruleList.back().first);
}

ExpressionPtr   InputParser::parseExpression(std::string & str) {
    ExpressionPtr       result;
    eOperatorPriority   currentPriority = eOperatorPriority::Brackets;

    while (currentPriority != eOperatorPriority::Fact) {
        switch (currentPriority) {
        case eOperatorPriority::Brackets:
            if (!parseBracketsExpression(str))
                currentPriority = eOperatorPriority::Not;
            else
                break;
        case eOperatorPriority::Not:
            if (!parseUnaryExpression(str, '!', eOperation::NOT))
                currentPriority = eOperatorPriority::And;
            else
                break;
        case eOperatorPriority::And:
            if (!parseBinaryExpression(str, '+', eOperation::AND))
                currentPriority = eOperatorPriority::Or;
            else
                break;
        case eOperatorPriority::Or:
            if (!parseBinaryExpression(str, '|', eOperation::OR))
                currentPriority = eOperatorPriority::Xor;
            else
                break;
        case eOperatorPriority::Xor:
            if (!parseBinaryExpression(str, '^', eOperation::XOR))
                currentPriority = eOperatorPriority::Fact;
            else
                break;
        case eOperatorPriority::Fact:
            break;
        }
    }

    if ((result = extractExpression(str, 0)) == nullptr || !str.empty()) {
        prepareRawInputString(str);
        std::string errMsg = "Syntax error occurred while parsing operands";
        if (str.empty() || std::find(m_allowedFacts.begin(), m_allowedFacts.end(), str[0]) != m_allowedFacts.end())
            errMsg += " - operator missed";
        if (!str.empty())
            errMsg += " at <" + str + ">";

        throw InputError(std::move(errMsg));
    }
    return (result);
}

bool            InputParser::parseBracketsExpression(std::string &str) {
    size_t      openBracket = str.rfind('(');
    if (openBracket == std::string::npos)
        return (false);
    size_t      closingBracket = str.find(')', openBracket);
    if (closingBracket == std::string::npos)
        throw InputError("Couldn't find closing bracket");
    std::string     expr = str.substr(openBracket + 1, closingBracket - openBracket - 1);
    if (expr.empty())
        throw InputError("Expression inside brackets cannot be empty");
    m_temp.push_back(parseExpression(expr));
    m_temp.back()->setInBrackets(true);
    std::string     placeHolder = "#" + std::to_string(m_temp.size() - 1) + "#";
    str.replace(openBracket, closingBracket - openBracket + 1, placeHolder);
    return (true);
}

bool InputParser::parseUnaryExpression(std::string &str, char oper, eOperation operation) {
    size_t      position = str.find(oper);
    if (position == std::string::npos)
        return (false);
    if (position == str.size() - 1)
        throw InputError("Missed operand in NOT operator");

    ExpressionPtr   expr = extractExpression(str, position + 1, true);
    m_temp.push_back(std::make_shared<UnaryExpression>(expr, operation));
    std::string     placeHolder = "#" + std::to_string(m_temp.size() - 1) + "#";
    str.replace(position, 1, placeHolder);
    return (true);
}

bool            InputParser::parseBinaryExpression(std::string &str, char oper, eOperation operation) {
    size_t      position = str.find(oper);

    if (position == std::string::npos)
        return (false);
    if (position == 0 || position == str.size() - 1)
        throw InputError("Missed operand in AND operator");

    ExpressionPtr   rhs = extractExpression(str, position + 1, true);
    ExpressionPtr   lhs = extractExpression(str, position - 1, false);
    m_temp.push_back(std::make_shared<BinaryExpression>(lhs, rhs, operation));

    std::string     placeHolder = "#" + std::to_string(m_temp.size() - 1) + "#";
    str.replace(str.find(oper), 1, placeHolder);
    return (true);
}

ExpressionPtr   InputParser::extractExpression(std::string & str, size_t position, bool frontDirection) {
    ExpressionPtr   result;

    if (position >= str.size())
        throw InputError("Syntax error");
    if (str[position] == '#')
    {
        size_t  start = frontDirection ? position : str.rfind('#', position - 1);
        size_t  end = frontDirection ? str.find('#', position + 1) : position;

        if (end == std::string::npos || start == std::string::npos || end - start <= 1)
            throw std::runtime_error("[Internal][extractExpression] incorrect expression link");

        size_t  index = std::stoul(str.substr(start + 1, end - start - 1));
        if (index >= m_temp.size())
            throw std::runtime_error("[Internal][extractExpression] Expression index > temporary container size");

        result = m_temp[index];
        m_temp[index].reset();
        str.erase(start, end - start + 1);
    }
    else if (str[position] < 'A' || str[position] > 'Z')
    {
        throw InputError(std::string("Syntax error at <") + str[position] + ">");
    }
    else {
        if ((result = m_input->factMap[str[position]]) == nullptr)
            result = m_input->factMap[str[position]] = std::make_shared<Fact>(str[position]);
        str.erase(position, 1);
    }
    return (result);
}

void        InputParser::parseFactString(std::string &str, eReadingState state) {
    if (str.empty())
        throw std::runtime_error("Fact string is empty");

    /* I know it isn't necessary to do it this way, just having fun */
    auto it = std::remove_if(str.begin() + 1, str.end(),
                             [](char c) { return (m_allowedFacts.find(c) == std::string::npos); });

    if (it != str.end())
        throw InputError(std::string("Invalid fact name - <") + *it + ">");
    clearFactStates(state);
    for (it = str.begin() + 1; it != str.end(); it++) {
        if (m_input->factMap[*it] == nullptr)
            m_input->factMap[*it] = std::make_shared<Fact>(*it);

        if (state == eReadingState::Facts) {
            m_input->factMap[*it]->setExprResult(eExprResult::True);
        } else if (state == eReadingState::Queries) {
            m_input->queries.push_back(m_input->factMap[*it]);
        }
        else
            throw std::runtime_error("[Internal][parseFactString] Wrong reading state passed");
    }
}

void        InputParser::proceedFinalChecks() const {
    if (m_state == eReadingState::Rules)
        throw InputError("Facts are missed");
    if (m_state == eReadingState::Queries)
        throw InputError("Queries are missed");
    if (m_input->queries.empty())
        throw InputError("Queries are empty");
}

InputPtr    InputParser::input() const {
    return (m_input == nullptr ? nullptr : createDeepCopy());
}

InputPtr    InputParser::createDeepCopy() const {
    auto copy = std::make_unique<tInput>();

    for (auto & item : m_input->factMap) {
        copy->factMap[item.first] = std::make_shared<Fact>(*item.second);
    }

    for (auto & factIt : m_input->queries) {
        auto destFactIt = std::find_if(copy->factMap.begin(), copy->factMap.end(), [factIt](const std::pair<char, FactPtr> & current) {
            return (factIt->toString() == current.second->toString());
        });
        copy->queries.push_back(destFactIt->second);
    }

    for (auto & rule : m_input->ruleList) {
        copy->ruleList.push_back(std::pair<ExpressionPtr, ExpressionPtr>
                (createDeepExprCopy(copy->factMap, rule.first), createDeepExprCopy(copy->factMap, rule.second)));
    }
    return (std::move(copy));
}

ExpressionPtr   InputParser::createDeepExprCopy(FactMap & factMap, ExpressionPtr &expr) const {
    switch (expr->type()) {
    case eExprType::Fact: {
        auto fact = std::dynamic_pointer_cast<Fact>(expr);
        if (fact == nullptr)
            throw std::runtime_error("[internal][createDeepExprCopy] Failed to cast pointer to Fact");
        return (factMap[fact->toString()[0]]);
    }
    case eExprType::Unary: {
        auto unaryExpr = std::dynamic_pointer_cast<UnaryExpression>(expr);
        if (unaryExpr == nullptr)
            throw std::runtime_error("[internal][createDeepExprCopy] Failed to cast pointer to Unary expression");
        auto unaryChild = unaryExpr->getChildren();
        if (unaryChild.size() != 1)
            throw std::runtime_error("[internal][createDeepExprCopy] Unary expression child list size != 1");
        auto unaryPtrNew = createDeepExprCopy(factMap, unaryChild.front());
        return (std::make_shared<UnaryExpression>(unaryPtrNew, *unaryExpr));
    }
    case eExprType::Binary: {
        auto binaryExpr = std::dynamic_pointer_cast<BinaryExpression>(expr);
        if (binaryExpr == nullptr)
            throw std::runtime_error("[internal][createDeepExprCopy] Failed to cast pointer to Binary expression");
        auto binaryChildren = binaryExpr->getChildren();
        if (binaryChildren.size() != 2)
            throw std::runtime_error("[internal][createDeepExprCopy] Binary expression children list size != 2");
        return (std::make_shared<BinaryExpression>
                (createDeepExprCopy(factMap, *binaryChildren.begin()),
                 createDeepExprCopy(factMap, *++binaryChildren.begin()), *binaryExpr));
    }
    default:
        throw std::runtime_error("[internal][createDeepExprCopy] Unhandled case");
    }
}

void            InputParser::clearFactStates(eReadingState state) {
    if (state == eReadingState::Queries)
        return m_input->queries.clear();

    for (const std::pair<const char, FactPtr> & mapItem : m_input->factMap) {
        if (mapItem.second->isDefined() || mapItem.second->getExprResult() == eExprResult::Undefined)
            m_input->factMap[mapItem.first].reset(new Fact(mapItem.first));
    }
}

void            InputParser::dumpInput() const {
    dumpRuleMap(m_input->ruleList);
    dumpFactMap(m_input->factMap);
    dumpQueryMap(m_input->queries);
}

void            InputParser::dumpRuleMap(const RuleList &ruleList) const {
    std::cout << "Rule list ";
    if (ruleList.empty())
        std::cout << "is empty" << std::endl;
    else
        std::cout << " (" << ruleList.size() << "):" << std::endl;
    size_t ruleNumber = 1;
    for (auto & a : ruleList) {
        if (a.first != nullptr && a.second != nullptr)
            std::cout << ruleNumber << ") If " << a.first->toString() << ", then " << a.second->toString() << std::endl;
        ruleNumber++;
    }
}

void            InputParser::dumpFactMap(const FactMap &map) const {
    size_t  count = 0;

    std::cout << "List of facts, set to TRUE:";
    for (const std::pair<const char, FactPtr> & a : map) {
        if (a.second->getExprResult() == eExprResult::True) {
            count++;
            std::cout << " " << a.first;
        }
    }
    if (count == 0) {
        std::cout << " empty";
    }
    std::cout << std::endl;
}

void            InputParser::dumpQueryMap(const FactList &queryList) const {
    std::cout << "Queries list ";
    if (queryList.empty())
        std::cout << "is empty" << std::endl;
    else
        std::cout << "contains " << queryList.size() << " items:";
    for (auto & a : queryList) {
        if (a != nullptr)
            std::cout << " " << a->toString();
    }
    std::cout << std::endl;
}

void            InputParser::deleteRule(size_t ruleNumber) {
    if (ruleNumber == 0 || ruleNumber - 1 >= m_input->ruleList.size())
        throw InputError(std::string("There is no rule with number ") + std::to_string(ruleNumber));
    auto it = m_input->ruleList.begin();
    std::advance(it, ruleNumber - 1);
    m_input->ruleList.erase(it);
}
