#ifndef INPUTPARSER_HPP
# define INPUTPARSER_HPP

# include <unordered_map>
# include <list>
# include <fstream>
# include <algorithm>
# include <deque>
# include "Fact.hpp"
# include "IExpression.hpp"
# include "InputError.hpp"

typedef std::unordered_map<char, FactPtr>                   FactMap;
typedef std::list<std::pair<ExpressionPtr, ExpressionPtr>>  RuleList;

typedef struct  sInput {
    FactMap     factMap;
    RuleList    ruleList;
    FactList    queries;
}               tInput;

typedef std::unique_ptr<tInput> InputPtr;

class InputParser final {
public:
    enum class eReadingState {
        Rules,
        Facts,
        Queries,
        Finished
    };

    InputParser(const InputParser & rhs) = delete;
    InputParser &	operator=(const InputParser & rhs) = delete;
    virtual ~InputParser() = default;

    static InputParser *    instance();
    InputPtr                parseFile(const char *fileName, bool isInteractive = false);
    InputPtr                input() const;
    /* For interactive mode */
    void                    dumpInput() const;      /* Nicely display saved input */
    void                    prepareRawInputString(std::string &str);
    void                    parseRule(std::string &str);
    void                    parseFactString(std::string &str, eReadingState state);
    void                    deleteRule(size_t ruleNumber);


private:
    enum class eOperatorPriority {
        Brackets,
        Not,
        And,
        Or,
        Xor,
        Fact,
    };

    InputParser() = default;
    void            proceedInputString(std::string & str);
    ExpressionPtr   parseExpression(std::string & str);
    bool            parseBracketsExpression(std::string &str);
    bool            parseUnaryExpression(std::string &str, char oper, eOperation operation);
    bool            parseBinaryExpression(std::string &str, char oper, eOperation operation);
    ExpressionPtr   extractExpression(std::string &str, size_t position, bool frontDirection = true);
    void            proceedFinalChecks() const;
    /* Creates deep copy of input */
    InputPtr        createDeepCopy() const;
    ExpressionPtr   createDeepExprCopy(FactMap & factMap, ExpressionPtr & expr) const;
    void            clearFactStates(eReadingState state);
    void            dumpRuleMap(const RuleList & ruleList) const;
    void            dumpFactMap(const FactMap & map) const;
    void            dumpQueryMap(const FactList & queryList) const;

    static std::unique_ptr<InputParser> m_instance;
    static const std::string            m_allowedFacts;
    static const std::string            m_allowedInput;
    std::unique_ptr<tInput>             m_input;
    eReadingState                       m_state;
    std::deque<ExpressionPtr>           m_temp;
    unsigned int                        m_stringNumber;
};

#endif
