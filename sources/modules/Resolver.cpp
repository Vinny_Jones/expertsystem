/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Resolver.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:31:39 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:31:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <exceptions/ResolverError.hpp>
#include "Resolver.hpp"

std::unique_ptr<Resolver> Resolver::m_instance = nullptr;

Resolver *      Resolver::instance() {
    if (m_instance == nullptr)
        m_instance.reset(new Resolver);
	return (m_instance.get());
}

void            Resolver::run(InputPtr && input) {
    m_input = std::move(input);
    m_factQueue = m_input->queries;
    m_ruleFactMap.clear();
    m_processedRulesMap.clear();

    if (m_input->queries.empty())
        throw (ResolverError("Queries can't be empty"));
    processQueue();

    std::cout << "=================" << std::endl << "Result:" << std::endl;
    for (auto & it : m_input->queries) {
        std::cout << it->toString() << " is " << IExpression::resultToString(it->getExprResult()) << std::endl;;
    }
}

void            Resolver::processQueue() {
    FactPtr ptr = m_factQueue.front();

    processFact(ptr);

    if (ptr == m_factQueue.front() && (ptr->isDefined() || ptr->getExprResult() == eExprResult::Undefined)) {
        m_factQueue.pop_front();
    }
    else if (ptr == m_factQueue.front())
        throw std::runtime_error("[Internal error][processQueue] front ptr didn't changed and fact is NOT resolved");


    if (!m_factQueue.empty())
        processQueue();
}

void            Resolver::processFact(FactPtr &currentFact) {
    bool        isFound = false;
    auto &      processedRules = m_processedRulesMap[currentFact];

    if (currentFact == nullptr)
        throw (ResolverError("Fact can't be empty"));
    for (auto & rule : m_input->ruleList) {
        if (isInExpression(currentFact, rule.second)) {
            eExprResult oldValue = currentFact->getExprResult();

            if (rule.first->getExprResult() == eExprResult::Undefined)
                continue;

            if (std::find(processedRules.begin(), processedRules.end(), rule.first) == processedRules.end()) {
                if (!rule.first->isDefined())
                    isFound = true;
                processedRules.insert(processedRules.end(), rule.first);
            }
            else {
                if (rule.first->getExprResult() == eExprResult::True) {
                    rule.second->setExprResult(eExprResult::True);
                    isFound = true;
                    printFactComparison(oldValue, currentFact->getExprResult(), rule.first->toString(), currentFact->toString());
                    continue;
                }
            }

            if (rule.first->isDefined()) {
                if (rule.first->getExprResult() == eExprResult::True) {
                    rule.second->setExprResult(eExprResult::True);
                    isFound = true;
                    printFactComparison(oldValue, currentFact->getExprResult(), rule.first->toString(), currentFact->toString());
                }
            }
            else if (isFound) {
                if (m_ruleFactMap[rule.first].empty()) {
                    mapExpressionToFacts(rule.first, rule.first);
                }

                for (auto & fact : m_ruleFactMap[rule.first]) {
                    m_factQueue.push_front(fact);
                }
            }
        }
    }

    if (!isFound && !currentFact->isDefined()) {
        std::cout << "Couldn't find rules, regarding fact " << currentFact->toString() << ". Set to False" << std::endl;
        currentFact->setExprResult(eExprResult::False);
    }
}

void            Resolver::printFactComparison(eExprResult oldValue, eExprResult newValue, std::string &&expr, std::string &&fact) const {
    std::cout << "Expression [" << expr << "] is True. Fact " << fact;
    std::cout << (oldValue != newValue ? " changed to " : " remains ");
    std::cout << IExpression::resultToString(newValue) << std::endl;
}

bool            Resolver::isInExpression(FactPtr &fact, ExpressionPtr &expr) {
    if (m_ruleFactMap[expr].empty()) {
        mapExpressionToFacts(expr, expr);
    }

    auto &  list = m_ruleFactMap[expr];
    return (std::find(list.begin(), list.end(), fact) != list.end());
}

void            Resolver::mapExpressionToFacts(ExpressionPtr &rule, ExpressionPtr &current) {
    if (rule == nullptr || current == nullptr)
        return;
    if (current->type() == eExprType::Fact) {
        auto &  list = m_ruleFactMap[rule];
        auto    fact = std::dynamic_pointer_cast<Fact>(current);

        if (fact == nullptr) {
            throw std::runtime_error("[Internal error][mapExpressionToFacts] fact == nullptr");
        }

        if (std::find(list.begin(), list.end(), current) == list.end()) {
            list.insert(list.end(), fact);
        }
    } else {
        for (auto & it : current->getChildren()) {
            mapExpressionToFacts(rule, it);
        }
    }
}
