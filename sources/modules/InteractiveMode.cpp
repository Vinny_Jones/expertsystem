/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InteractiveMode.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 19:31:28 by apyvovar          #+#    #+#             */
/*   Updated: 2018/07/31 19:31:31 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InteractiveMode.hpp"

std::unique_ptr<InteractiveMode>    InteractiveMode::m_instance = nullptr;

InteractiveMode* InteractiveMode::instance() {
    if (m_instance == nullptr)
        m_instance.reset(new InteractiveMode());
    return (m_instance.get());
}

bool    InteractiveMode::run() {
    std::cout << std::endl << "Interactive mode is on. ";
    showCommandsList();

    while (true) {
        switch (getUserCommand()) {
        case eModeState::RUN:
            std::cout << "Running... " << std::endl;
            return (true);
        case eModeState::ADD_RULE:
            performAddRule();
            break;
        case eModeState::DELETE_RULE:
            performDeleteRule();
            break;
        case eModeState::EDIT_FACTS:
            performEditFactList(InputParser::eReadingState::Facts);
            break;
        case eModeState::EDIT_QUERIES:
            performEditFactList(InputParser::eReadingState::Queries);
            break;
        case eModeState::EXIT:
            std::cout << "Exiting... " << std::endl;
            return (false);
        }
    }
}

void        InteractiveMode::performAddRule() {
    std::string buffer;

    std::cout << "Enter rules or E to finish: " << std::endl;
    while (getline(std::cin, buffer)) {
        try {
            if (buffer == "E" || buffer == "e")
                break;
            InputParser::instance()->prepareRawInputString(buffer);
            if (buffer.empty()) {
                std::cout << "Enter rules or E to finish: ";
                continue;
            }

            InputParser::instance()->parseRule(buffer);

        } catch (InputError & ex) {
            std::cout << "An error occurred while reading from input: " << ex.what() << ". Please, try again." << std::endl;
        }
    }
    std::cout << "All rules added successfully." << std::endl;
}

void        InteractiveMode::performDeleteRule() {
    std::string buffer;

    std::cout << "Enter fact number to delete (or E to exit): ";
    while (getline(std::cin, buffer)) {
        if (buffer == "E" || buffer == "e")
            return;
        if (buffer.empty()) {
            std::cout << "Enter fact number to delete (or E to exit): ";
            continue;
        }

        try {
            if (buffer.find_first_not_of("0123456789") != std::string::npos)
                throw InputError("Input must contain digits only");
            std::istringstream  stream(buffer);
            size_t  index;

            stream >> index;
            InputParser::instance()->deleteRule(index);
            std::cout << "Rule deleted." << std::endl;
            return;
        } catch (InputError & ex) {
            std::cout << "Wrong input: " << ex.what() << ". Please, enter valid rule number or E to exit: ";
        }
    }
}

void        InteractiveMode::performEditFactList(InputParser::eReadingState state) {
    std::string buffer;
    std::string editable = (state == InputParser::eReadingState::Facts ? "Facts" : "Queries");

    std::cout << "Enter ALL " << editable;
    if (state == InputParser::eReadingState::Facts)
        std::cout << ", that should be set to TRUE initially";
    std::cout << ": ";

    while (std::getline(std::cin, buffer)) {
        try {
            InputParser::instance()->prepareRawInputString(buffer);
            InputParser::instance()->parseFactString(buffer.insert(0, "#"), state);
            std::cout << editable << " set successfully" << std::endl;
            return;
        } catch (std::exception & ex) {
            std::cout << "Invalid input. Please, enter valid string of " << editable << ": ";
        }
    }
}

eModeState  InteractiveMode::getUserCommand() const {
    std::string buffer;

    std::cout << "What would you like to do: ";
    while (std::getline(std::cin, buffer)) {
        if (buffer == "A" || buffer == "a")
            return (eModeState::ADD_RULE);
        if (buffer == "D" || buffer == "d")
            return (eModeState::DELETE_RULE);
        if (buffer == "F" || buffer == "f")
            return (eModeState::EDIT_FACTS);
        if (buffer == "Q" || buffer == "q")
            return (eModeState::EDIT_QUERIES);
        if (buffer == "R" || buffer == "r")
            return (eModeState::RUN);
        if (buffer == "E" || buffer == "e")
            return (eModeState::EXIT);
        if (buffer == "S" || buffer == "s")
            InputParser::instance()->dumpInput();
        else if (buffer == "C" || buffer == "c")
            showCommandsList();
        else if (!buffer.empty())
            std::cout << "Wrong input. Please, enter valid command or \"C\" to list all commands." << std::endl;
        std::cout << "What would you like to do: ";
    }
    return (eModeState::EXIT);
}

void        InteractiveMode::showCommandsList() const {
    std::cout << "Commands: C - list commands, S - show input, A - add rule, D - delete rule, ";
    std::cout << "F - set initial facts, Q - set query, R - run with current input, E - exit." << std::endl << std::endl;
}
